#include <SoftwareSerial.h>

#define PIN_OPEN 4
#define PIN_CLOSE 2
#define ACTION_DURATION 60000 //seconds - time needed to open or close shutter
#define AUTO_CLOSE_TRESHOLD 300000 // 5 min in sec

//bluetooth
SoftwareSerial EEBlue(10, 11); // RX | TX
int open_count=0;
int close_count=0;

//logic
unsigned long auto_close_timer = 0;
unsigned long action_timer = 0;
bool is_opening = false;
bool is_closing = false;
bool is_motor_running = false;
bool is_open = false;

void setup()
{
  Serial.begin(9600);
  EEBlue.begin(9600);  //Default Baud for comm.
  pinMode(PIN_OPEN, OUTPUT);
  pinMode(PIN_CLOSE, OUTPUT);
  digitalWrite(PIN_OPEN, HIGH);
  digitalWrite(PIN_CLOSE, HIGH);
  auto_close_timer = millis();
}
 
void loop()
{
  action_timer_check();
  auto_close_check();
  bluetooth_routine();  
}

//controlla se apertura o chiusura sono finite
void action_timer_check(){
  if(action_timer != 0 && millis() - action_timer > ACTION_DURATION){
    if(is_opening){
      stop_opening();
    }
    else {
      stop_closing();
    }
  }
}

void auto_close_check(){
  if(is_open && millis() - auto_close_timer > AUTO_CLOSE_TRESHOLD){
    start_closing();
    auto_close_timer = millis();
  }
}

void start_closing(){
  if(is_opening){
    stop_opening();
  }
  action_timer = millis();
  digitalWrite(PIN_CLOSE, LOW);
  is_closing = true;
}

void start_opening(){
  if(is_closing){
    stop_closing();
  }
  action_timer = millis();
  auto_close_timer = millis();
  digitalWrite(PIN_OPEN, LOW);
  is_opening = true;
}

void stop_closing(){
  digitalWrite(PIN_CLOSE, HIGH);
  action_timer = 0;
  is_closing = false;
  is_open = false;
}

void stop_opening(){
  digitalWrite(PIN_OPEN, HIGH);
  action_timer = 0;
  is_opening = false;
  is_open = true;
}

void bluetooth_routine(){
  if(EEBlue.available()){
    char inChar = (char)EEBlue.read();
    Serial.println(inChar);
    if(inChar == 'O'){
      open_count++;
      close_count = 0;
      if(open_count == 3){
        start_opening();
      }
    }else if(inChar == 'C'){
      close_count++;
      open_count = 0;
      if(close_count == 3){
        start_closing();
      }
    }else if(inChar == 'K'){
      auto_close_timer = millis();
    }
  }
}

  /* utile per implementare ack e fine comunicazioni da arduino a raspberry   
  // Feed all data from termial to bluetooth
  if (Serial.available())
    EEBlue.write(Serial.read());
  */
